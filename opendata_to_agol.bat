echo off
rem get all parameters
set PARAMETERS= %*
set SCRIPTNAME=opendata_to_agol.py
set SCRIPT=%~dp0%SCRIPTNAME%
set ENV_YML=%~dp0environment.yml
set ENVNAME=pyarcgis
set CONDABIN_PATH_ESRI=C:\Program Files\ArcGIS\Pro\bin\Python\Scripts
set CONDABIN_PATH=%USERPROFILE%\Miniconda3\condabin
set ANACONDABIN_PATH=%USERPROFILE%\anaconda3\condabin
if not exist "%CONDABIN_PATH_ESRI%" if not exist "%CONDABIN_PATH%" if not exist "%ANACONDABIN_PATH%" goto :notinstalled
set PATH=%PATH%;%CONDABIN_PATH%;%ANACONDABIN_PATH%;%CONDABIN_PATH_ESRI%;
for /f "tokens=* delims=" %%i in ('call activate %ENVNAME% 2^>^&1') do (
   if "%%i"=="EnvironmentNameNotFound: Could not find conda environment: %ENVNAME%" call :create
   if "%%i"=="Could not find conda environment: %ENVNAME%" call :create
)
call :run
call :cleanup
goto :eof

:notinstalled
    echo ----------------------------------------------------------------------
    echo   No installation of conda installed in %CONDABIN_PATH%.
    call :miniconda
    pause
    exit /b
 
:miniconda
    echo   Install the latest 64-bit version of Miniconda using the default
    echo   installation options (e.g. Just for me, default folder, etc.).
    echo   https://docs.conda.io/en/latest/miniconda.html#latest-miniconda-installer-links
    echo ----------------------------------------------------------------------
    start "" https://docs.conda.io/en/latest/miniconda.html#latest-miniconda-installer-links
    exit /b

:create
    echo ----------------------------------------------------------------------
    echo   Environment %ENVNAME% not found.
    echo   Creating it now (subsequent runs will skip this step)...
    echo ----------------------------------------------------------------------
    call conda env create --file %ENV_YML% 2^>^&1
    exit /b

:run
    echo ----------------------------------------------------------------------
    echo   Running script %SCRIPTNAME%
    echo   in environment %ENVNAME%.
    echo ----------------------------------------------------------------------
    call activate %ENVNAME%
    echo python %SCRIPT% %PARAMETERS%
    call python %SCRIPT% %PARAMETERS%
    exit /b

:cleanup
    echo %~d0
    call deactivate
    exit /b

# -*- coding: utf-8 -*-
"""Global variables used by "opendata_to_agol.py"."""
# -----------
# STANDARD
import sys
import os
import getpass
import logging
from logging.handlers import TimedRotatingFileHandler
# -----------
# THIRD-PARTY
import requests
import arcgis
# -----------
# CUSTOM
from get_dept_dicts import get_dept_dicts

# ADD PATH TO CREDENTIALS FILE TO AVOID TYPING IN CREDENTIALS EACH RUN
# sys.path.append('\\\\path\\to\\credentials.py')
try:
    from credentials import USERNAME, PASSWORD
except ImportError:
    USERNAME = input('ArcGIS Online username: ')
    PASSWORD = getpass.getpass('Password for {}: '.format(USERNAME))
# -----------

NAME = 'opendata_to_agol'

THIS_FILE = os.path.abspath(__file__)
THIS_DIR = os.path.dirname(THIS_FILE)

DIR_DATA = os.path.join(THIS_DIR, 'data')
if not os.path.exists(DIR_DATA):
    os.makedirs(DIR_DATA)
CSW_FILE = os.path.join(DIR_DATA, 'elements_by_csw_id.dat')
AGOL_FILE = os.path.join(DIR_DATA, 'agol_items_by_csw_id_lang.dat')

AGOL_ITEM_URL = 'https://aafc.maps.arcgis.com/home/item.html?id='
OPENDATA_RECORDS_URL = (
    'https://csw.open.canada.ca/geonetwork/srv/csw?' +
    'service=CSW&version=2.0.2&request=GetRecords&' +
    'typeNames=gmd:MD_Metadata&' +
    'ElementSetName=full&' +
    'outputSchema=csw:IsoRecord&resultType=results')

OPENDATA_RECORD_URL = (
    'https://csw.open.canada.ca/geonetwork/srv/csw?' +
    'service=CSW&version=2.0.2&request=GetRecordById&' +
    'ElementSetName=full&' +
    'outputSchema=csw:IsoRecord&id=')

OPENDATA_RECORD_FORMATS = {}

TEST_SERVICE_URL = (
    'https://maps-cartes.services.geo.ca/server2_serveur2/' +
    'rest/services/BaseMaps/CBMT_CBCT_GEOM_3857/MapServer')

TEST_OPENDATA_URL = (
    OPENDATA_RECORD_URL + '84d1ff0d-4689-466c-b792-f860a1fb1603')

TEST_GCGEO_URL = (
    'https://gcgeo.gc.ca/geonetwork/srv/eng/xml.metadata.get?uuid=' +
    '84d1ff0d-4689-466c-b792-f860a1fb1603')

OPENDATA_ITEM_URL = {
    'EN': 'https://open.canada.ca/data/en/dataset/',
    'FR': 'https://ouvert.canada.ca/data/fr/dataset/'}

OPENDATA_LICENCE_URLS = {
    'EN': 'https://open.canada.ca/en/open-government-licence-canada',
    'FR': 'https://ouvert.canada.ca/fr/licence-du-gouvernement-ouvert-canada'}

GROUP_DESCRIPTIONS = {
    'EN': 'Mapping services provided by',
    'FR': 'Services de cartographie fournis par'}

NAMESPACES = {
    "gmd": "http://www.isotc211.org/2005/gmd",
    "gco": "http://www.isotc211.org/2005/gco",
    "srv": "http://www.isotc211.org/2005/srv",
    "gml": "http://www.opengis.net/gml/3.2",
    "xlink": "http://www.w3.org/1999/xlink",
    "gfc": "http://www.isotc211.org/2005/gfc",
    "gmi": "http://www.isotc211.org/2005/gmi",
    "gmx": "http://www.isotc211.org/2005/gmx",
    "gsr": "http://www.isotc211.org/2005/gsr",
    "gss": "http://www.isotc211.org/2005/gss",
    "gts": "http://www.isotc211.org/2005/gts",
    "geonet": "http://www.fao.org/geonetwork",
    "csw": "http://www.opengis.net/cat/csw/2.0.2",
    "dct": "http://purl.org/dc/terms/",
    "dc": "http://purl.org/dc/elements/1.1/"
}

FORMATS = ['ESRI REST', 'WMS', 'WFS', 'WCS']

# for records with multiple service types, this is the order of preference
PREFERRED_TYPES = [
    'ESRI REST: Map Server',
    'ESRI REST: Map Service',
    'ESRI REST: Image Server',
    'ESRI REST: Image Service',
    'ESRI REST',
    'OGC:WMS',
    'WMS',
    'OGC:WFS',
    'WFS',
    'OGC:WCS',
    'WCS']

ELEMENT_TAGS = {
    'metadata': ['csw:SearchResults/gmd:MD_Metadata'],
    'guid': ['.gmd:fileIdentifier/.gco:CharacterString'],
    'type': [
        '.gmd:distributionInfo/.gmd:MD_Distribution/.gmd:transferOptions/' +
        '.gmd:MD_DigitalTransferOptions/.gmd:onLine/.gmd:CI_OnlineResource/' +
        '.gmd:description'],
    'online': [
        '.gmd:distributionInfo/.gmd:MD_Distribution/.gmd:transferOptions/' +
        '.gmd:MD_DigitalTransferOptions/.gmd:onLine'],
    'url': [
        '.gmd:distributionInfo/.gmd:MD_Distribution/.gmd:transferOptions/' +
        '.gmd:MD_DigitalTransferOptions/.gmd:onLine/.gmd:CI_OnlineResource/' +
        '.gmd:linkage/.gmd:URL'],
    'date': [
        '.gmd:dateStamp/.gco:DateTime',
        '.gmd:dateStamp/.gco:Date'],
    'title': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:citation/.gmd:CI_Citation/.gmd:title'],
    'description': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/.gmd:abstract'],
    'snippet': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/.gmd:purpose'],
    'tags': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:descriptiveKeywords/.gmd:MD_Keywords/.gmd:keyword'],
    'licenceInfo': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:resourceConstraints/.gmd:MD_LegalConstraints/' +
        '.gmd:useLimitation'],
    'credits': [
        '.gmd:contact/.gmd:CI_ResponsibleParty/.gmd:organisationName'],
    'spatialReference': [
        '.gmd:referenceSystemInfo/.gmd:MD_ReferenceSystem/' +
        '.gmd:referenceSystemIdentifier/.gmd:RS_Identifier/.gmd:code'],
    'thumbnails': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:graphicOverview/.gmd:MD_BrowseGraphic'],
    'extent': [
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:extent/.gmd:EX_Extent/.gmd:geographicElement/' +
        '.gmd:EX_GeographicBoundingBox',
        '.gmd:identificationInfo/.gmd:MD_DataIdentification/' +
        '.gmd:extent/.gmd:EX_Extent/.gmd:geographicElement/' +
        '.gmd:GeographicBoundingBox'],
    'categories': [
        'gmd:identificationInfo/gmd:MD_DataIdentification/' +
        'gmd:topicCategory/gmd:MD_TopicCategoryCode']
    }

ADDITIONAL_ACRONYMS = {
    'Government of British Columbia': {'EN': 'BC', 'FR': 'BC'},
    'Gouvernement de la Colombie-Britannique': {'EN': 'BC', 'FR': 'BC'},
    'Government of Alberta': {'EN': 'AB', 'FR': 'AB'},
    'Gouvernement de l\'Alberta': {'EN': 'AB', 'FR': 'AB'},
    'Government of Manitoba': {'EN': 'MB', 'FR': 'MB'},
    'Gouvernement du Manitoba': {'EN': 'MB', 'FR': 'MB'},
    'Government of Ontario':  {'EN': 'ON', 'FR': 'ON'},
    'Gouvernement de l\'Ontario': {'EN': 'ON', 'FR': 'ON'},
    'Government of New Brunswick': {'EN': 'NB', 'FR': 'NB'},
    'Gouvernement du Nouveau-Brunswick': {'EN': 'NB', 'FR': 'NB'},
    'Government of Newfoundland and Labrador': {'EN': 'NL', 'FR': 'NL'},
    'Gouvernement de Terre-Neuve-et-Labrador': {'EN': 'NL', 'FR': 'NL'},
    'Government of the Northwest Territories': {'EN': 'NT', 'FR': 'NT'},
    'Gouvernement des Territoires du Nord-Ouest': {'EN': 'NT', 'FR': 'NT'},
    'Government of Nova Scotia': {'EN': 'NS', 'FR': 'NS'},
    'Gouvernement de la Nouvelle-Écosse': {'EN': 'NS', 'FR': 'NS'},
    'Government of Nunavut': {'EN': 'NU', 'FR': 'NU'},
    'Gouvernement du Nunavut': {'EN': 'NU', 'FR': 'NU'},
    'Government of Prince Edward Island': {'EN': 'PE', 'FR': 'PE'},
    'Gouvernement de l\'Île-du-Prince-Édouard': {'EN': 'PE', 'FR': 'PE'},
    'Government of Quebec': {'EN': 'QC', 'FR': 'QC'},
    'Government of Québec': {'EN': 'QC', 'FR': 'QC'},
    'Government and Municipalities of Québec': {'EN': 'QC', 'FR': 'QC'},
    'Gouvernement du Québec': {'EN': 'QC', 'FR': 'QC'},
    'Gouvernement et municipalités du Québec': {'EN': 'QC', 'FR': 'QC'},
    'Government of Saskatchewan': {'EN': 'SK', 'FR': 'SK'},
    'Gouvernement de la Saskatchewan': {'EN': 'SK', 'FR': 'SK'},
    'Government of Yukon': {'EN': 'YT', 'FR': 'YT'},
    'Gouvernement du Yukon': {'EN': 'YT', 'FR': 'YT'},
    'Transports Canada': {'EN': 'TC', 'FR': 'TC'}}

ADDITIONAL_SOURCES_BY_LANG = {
    'EN': [
        'Government of British Columbia',
        'Government of Alberta',
        'Government of Manitoba',
        'Government of Ontario',
        'Government of New Brunswick',
        'Government of Newfoundland and Labrador',
        'Government of the Northwest Territories',
        'Government of Nova Scotia',
        'Government of Nunavut',
        'Government of Prince Edward Island',
        'Government of Quebec',
        'Government of Québec',
        'Government and Municipalities of Québec',
        'Government of Saskatchewan',
        'Government of Yukon',
    ],
    'FR': [
        'Gouvernement de la Colombie-Britannique',
        'Gouvernement de l\'Alberta',
        'Gouvernement du Manitoba',
        'Gouvernement de l\'Ontario',
        'Gouvernement du Nouveau-Brunswick',
        'Gouvernement de Terre-Neuve-et-Labrador',
        'Gouvernement des Territoires du Nord-Ouest',
        'Gouvernement de la Nouvelle-Écosse',
        'Gouvernement du Nunavut',
        'Gouvernement de l\'Île-du-Prince-Édouard',
        'Gouvernement du Québec',
        'Gouvernement et municipalités du Québec',
        'Gouvernement de la Saskatchewan',
        'Gouvernement du Yukon',
    ]}

# category translations from:
#  https://inspire.ec.europa.eu/metadata-codelist/TopicCategory

# apostrophies are not allowed for ArcGIS Online category names, so
# "Services d'utilité publique et communication" was shortened to
# "Services et communication"

CATEGORIES = {
    'biota': {
        'EN': u'Biota',
        'FR': u'Biote'},
    'boundaries': {
        'EN': u'Boundaries',
        'FR': u'Limites'},
    'climatologyMeteorologyAtmosphere': {
        'EN': u'Climatology, meteorology, and atmosphere',
        'FR': u'Climatologie, météorologie et atmosphère'},
    'economy': {
        'EN': u'Economy',
        'FR': u'Économie'},
    'elevation': {
        'EN': u'Elevation',
        'FR': u'Altitude'},
    'environment': {
        'EN': u'Environment',
        'FR': u'Environnement'},
    'farming': {
        'EN': u'Farming',
        'FR': u'Agriculture'},
    'geoscientificInformation': {
        'EN': u'Geoscientific information',
        'FR': u'Informations géoscientifiques'},
    'health': {
        'EN': u'Health',
        'FR': u'Santé'},
    'imageryBaseMapsEarthCover': {
        'EN': u'Imagery, basemaps, and Earth cover',
        'FR': u'Imageries, cartes de base et occupation des terres'},
    'inlandWaters': {
        'EN': u'Inland waters',
        'FR': u'Eaux intérieures'},
    'intelligenceMilitary': {
        'EN': u'Intelligence and military',
        'FR': u'Renseignements et secteur militaires'},
    'location': {
        'EN': u'Location',
        'FR': u'Localisation'},
    'oceans': {
        'EN': u'Oceans',
        'FR': u'Océans'},
    'planningCadastre': {
        'EN': u'Planning and cadastre',
        'FR': u'Planification et cadastre'},
    'society': {
        'EN': u'Society',
        'FR': u'Société'},
    'structure': {
        'EN': u'Structure',
        'FR': u'Structures'},
    'transportation': {
        'EN': u'Transportation',
        'FR': u'Transport'},
    'transport': {
        'EN': u'Transportation',
        'FR': u'Transport'},
    'utilitiesCommunication': {
        'EN': u'Utilities and communication',
        'FR': u'Services et communication'}}


def create_session():
    """Create a requests session."""
    session = requests.session()
    retries = requests.adapters.Retry(
        total=5, backoff_factor=1, status_forcelist=[502, 503, 504])
    session.mount(
        prefix='https://',
        adapter=requests.adapters.HTTPAdapter(max_retries=retries))
    return session


SESSION = create_session()


def get_logger(name=None):
    """
    Return a logger with file and stream handlers.

    Creates a new logger if the logger "name" does not exist.
    """
    this_file = os.path.abspath(__file__)
    this_dir = os.path.dirname(this_file)

    if name:
        logger_name = name
    else:
        logger_name = os.path.basename(this_file.split('.')[0])
    logger = logging.getLogger(logger_name)
    if logger.handlers:
        return logger

    log_dir = os.path.join(this_dir, 'logs')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    logfilename = '{}.log'.format(logger_name)
    logfile = os.path.join(log_dir, logfilename)
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(levelname)s: %(asctime)s : %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    file_handler = TimedRotatingFileHandler(
        logfile, when='d', interval=1, backupCount=30)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    return logger


LOGGER = get_logger(name=NAME)


def get_gis_object():
    """
    Create the GIS user object.

    This allows us to refresh the GIS user after a change was made to
    the account. In particular, creating a new group will not refresh
    the GIS.users.me.groups property.
    """
    return arcgis.GIS(username=USERNAME, password=PASSWORD)


GIS = get_gis_object()
if not GIS:
    LOGGER.error('Could not create arcgis object.')
    sys.exit()

ACRONYMS_BY_NAME, CREDITS_BY_LANGUAGE = get_dept_dicts()
ACRONYMS_BY_NAME.update(ADDITIONAL_ACRONYMS)

for lang in ['EN', 'FR']:
    for source in ADDITIONAL_SOURCES_BY_LANG[lang]:
        CREDITS_BY_LANGUAGE[lang].append(source)

# -*- coding: utf-8 -*-
"""
Add items from Open Data to ArcGIS Online.

Find all web mapping services on the public Open Data site and
add them to ArcGIS Online as new items within folders named after official
department acroynms (e.g. "AAFC" for Agricultural and Agri-Food Canada,
"TC" for Transport Canada, etc.).

Command-line only.

Requirements:
- Python 3.8+
- ArcGIS Online account

Parameters:
--all, -a
    Add Open Data items from all departments.
--debug, -db
    Run in debug mode.
--delete, -d
    Delete obsolete ArcGIS Online items.
--doctest, -dt
    Run the doctests.
--file, -f
    Load Open Data content saved from the previous run.
--org, -o
    Specific organization acronym with Open Data items.
--status, -s
    Compare Open Data content with AGOL content. No items added.
--update, -u
    Update existing ArcGIS Online items only, do not add new items.
--validate, -v
    Validate existing ArcGIS Online items.
"""
import os
import re
import time
import datetime
import argparse
import pickle
import webbrowser
import urllib.request
import urllib.error
import urllib.parse

import xml.etree.ElementTree as ET
import logging

import arcgis

# All uppercase variables are from "globals.py"
from globals import ACRONYMS_BY_NAME, AGOL_ITEM_URL, CATEGORIES, \
    CREDITS_BY_LANGUAGE, CSW_FILE, ELEMENT_TAGS, GIS, \
    GROUP_DESCRIPTIONS, LOGGER, NAMESPACES, OPENDATA_ITEM_URL, \
    OPENDATA_LICENCE_URLS, OPENDATA_RECORDS_URL, OPENDATA_RECORD_URL, \
    PREFERRED_TYPES, SESSION, TEST_OPENDATA_URL, TEST_SERVICE_URL, USERNAME


def parse_args():
    """Parse arguments passed to this script."""
    parser = argparse.ArgumentParser(
        description='Copy Open Data items to ArcGIS Online')
    parser.add_argument(
        '--all', '-a', action='store_true', default=False,
        help='Add Open Data items from all departments.')
    parser.add_argument(
        '--debug', '-db', action='store_true', default=False,
        help='Run in debug mode.')
    parser.add_argument(
        '--delete', '-d', action='store_true', default=False,
        help='Delete obsolete ArcGIS Online items.')
    parser.add_argument(
        '--doctest', '-dt', action='store_true', default=False,
        help='Run the doctests.')
    parser.add_argument(
        '--file', '-f', action='store_true', default=False,
        help='Load Open Data content saved from the previous run.')
    parser.add_argument(
        '--org', '-o', dest='org', nargs='?', default=None, type=str,
        help='Specific organization acronym with Open Data items.')
    parser.add_argument(
        '--status', '-s', action='store_true', default=False,
        help='Compare Open Data content with AGOL content. No items added.')
    parser.add_argument(
        '--update', '-u', action='store_true', default=False,
        help='Update existing ArcGIS Online items only, do not add new items.')
    parser.add_argument(
        '--validate', '-v', action='store_true', default=False,
        help='Validate existing ArcGIS Online items.')
    return parser.parse_args()


def create_test_item():
    """Create a temporary AGOL item for testing."""
    item = GIS.content.add(
        item_properties={
            'title': 'opendata_to_agol',
            'type': 'Map Service',
            'url': TEST_SERVICE_URL,
            'tags': ['test_csw_id']})
    return item


def delete_agol_item(agol_id):
    """Delete AGOL item."""
    item = arcgis.gis.Item(GIS, agol_id)
    result = item.delete()
    return result


def get_uids_in_text(text, hyphens=True):
    """
    Find uid strings in text, both standard and non-standard.

    :param text: text string
    :parm hyphens: only find uids containing hyphens
    :returns: a list of uid strings

    Examples:
    standard uid strings:
    3e9cc0c9-7e59-478b-befb-a584674baa75 or 4733cec86bc6471a8a8c24d5db620e96

    non-standard uid strings:
    68d87b4f-6e7d-9432-2a78-a5d8f0a86a11
                  ^    ^
    >>> text = '''xxxxx3e9cc0c9-7e59-478b-befb-a584674baa75 xxxxxxxx
    ... xxxxxx68d87b4f-6e7d-9432-2a78-a5d8f0a86a11 xxxx'''
    >>> result = get_uids_in_text(text)
    >>> len(result)
    2
    >>> '3e9cc0c9-7e59-478b-befb-a584674baa75' in result
    True
    >>> '68d87b4f-6e7d-9432-2a78-a5d8f0a86a11' in result
    True
    >>> get_uids_in_text('t4733cec86bc6471a8a8c24d5db620e96', hyphens=False)
    ['4733cec86bc6471a8a8c24d5db620e96']
    """
    uids = []
    # standard (not used)
    regex = (
        '[0-9a-f]{8}-?' +
        '[0-9a-f]{4}-?' +
        '[1-5][0-9a-f]{3}-?' +
        '[89ab][0-9a-f]{3}-?' +
        '[0-9a-f]{12}')
    # non-standard
    regex = (
        '[0-9a-f]{8}-?' +
        '[0-9a-f]{4}-?' +
        '[0-9a-f]{4}-?' +
        '[0-9a-f]{4}-?' +
        '[0-9a-f]{12}')

    result = re.search(regex, text)
    if result:
        uid = text[result.start(): result.end()]
        while uid:
            if hyphens and '-' not in uid:
                pass
            else:
                uids.append(uid)
            text = text[result.end():]
            result = re.search(regex, text)
            if not result:
                break
            uid = text[result.start(): result.end()]
    return list(set(uids))


def get_element_tree(url, request='get'):
    """
    Get an XML element tree from a request response for a URL.

    :param url: a URL that returns an XML document
    :param request: type of requests, get or post

    :return: element tree object

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> et.find(
    ...     ELEMENT_TAGS['title'][0] + '/.gco:CharacterString',
    ...     NAMESPACES).text
    'Crop Development Stage'
    """
    if request == 'get':
        response = SESSION.get(url)
    else:
        response = SESSION.post(url)
    xml_string = response.text
    if not xml_string:
        return None
    root_element_tree = ET.ElementTree(ET.fromstring(xml_string))
    if not root_element_tree:
        return None
    element_tree = root_element_tree.find('.gmd:MD_Metadata', NAMESPACES)
    if not element_tree:
        return root_element_tree
    return element_tree


def get_full_csw_record(csw_id):
    """
    Get the full CSW (Open Data) XML record.

    :param csw_id: unique identifier for data record
    :return: element tree object

    >>> et = get_full_csw_record(TEST_OPENDATA_URL.split('=')[-1])
    >>> et.find(
    ...     ELEMENT_TAGS['title'][0] + '/.gco:CharacterString',
    ...     NAMESPACES).text
    'Crop Development Stage'
    """
    url = OPENDATA_RECORD_URL + csw_id
    root_element_tree = get_element_tree(url)
    if not root_element_tree:
        LOGGER.error('... failed getting record count')
        return None
    element_tree = root_element_tree.find('.gmd:MD_Metadata', NAMESPACES)
    if not element_tree:
        return root_element_tree
    return element_tree


def get_first_value(element_record, tagname, lang=None):
    """
    Get the first value of an element tag by language.

    This will return the English value unless it does not exist, then
    it will return the French value.

    :param values_by_lang: dictionary of lists by language
    :return: the first value
    """
    values_by_lang = get_element_value(element_record, tagname)
    if not values_by_lang:
        return ''
    if lang:
        if lang in values_by_lang and values_by_lang[lang]:
            return values_by_lang[lang][0]
        return ''
    if 'EN' in values_by_lang and values_by_lang['EN']:
        return values_by_lang['EN'][0]
    if 'FR' in values_by_lang and values_by_lang['FR']:
        return values_by_lang['FR'][0]
    return ''


def get_open_data_records():
    """
    Get last modified dates of csw items in csw.open.canada.ca.

    :param org: organization name (optional)
    :return: dictionary of date values with csw ids as the keys
    """
    open_data_records = {}
    start = 1
    # DO NOT FILTER BY 'FORMAT' SINCE SOME RECORDS USE THE FORMAT
    # VALUE OF 'HTML' TO REPRESENT 'WMS', 'ESRI REST', ETC.
    # AVOID THE NEXT THREE PARAMETERS FOR NOW
    # 'constraintLanguage': 'CQL_TEXT',
    # 'CONSTRAINT_LANGUAGE_VERSION': '1.1.0',
    # 'Constraint': ' or '.join(
    #     ['Format = \'' + format + '\'' for format in FORMATS])}
    url = f'{OPENDATA_RECORDS_URL}&maxRecords=1&startPosition={start}'

    element_tree = get_element_tree(url)
    if not element_tree:
        LOGGER.error('... failed getting record count')
        return None
    records = 0
    for element in element_tree.iter():
        if element.tag.endswith('SearchResults'):
            records = int(element.attrib['numberOfRecordsMatched'])
            break
    LOGGER.info('Searching through %s items on Open Data...', records)
    for i in range(0, records + 1, 100):
        start = i + 1

        if not i % 500:
            LOGGER.info('... %s', i)
        url = f'{OPENDATA_RECORDS_URL}&maxRecords=100&startPosition={start}'
        element_tree = get_element_tree(url)
        if not element_tree:
            LOGGER.error('.... failed retrieving records')
            return None
        element_records = element_tree.findall(
            ELEMENT_TAGS['metadata'][0], NAMESPACES)
        for element_record in element_records:
            csw_id = get_first_value(element_record, 'guid')
            if not csw_id:
                continue
            service_types_by_lang, _ = get_service_types_and_urls(
                element_record)
            if not service_types_by_lang:
                continue
            if 'EN' in service_types_by_lang and service_types_by_lang['EN']:
                service_type = service_types_by_lang['EN'][0]
            elif 'FR' in service_types_by_lang and service_types_by_lang['FR']:
                service_type = service_types_by_lang['FR'][0]
            else:
                continue
            if service_type not in PREFERRED_TYPES:
                continue
            datestr = get_first_value(element_record, 'date')
            if not datestr:
                continue
            try:
                date = datetime.datetime.strptime(
                    datestr, '%Y-%m-%dT%H:%M:%S').timestamp()
            except ValueError:
                date = datetime.datetime.strptime(
                    datestr, '%Y-%m-%d').timestamp()

            source_credits = get_first_value(element_record, 'credits')

            if not source_credits:
                continue
            credit_list = [
                credit.strip() for credit in source_credits.split(';')]
            if not credit_list:
                continue
            if csw_id not in open_data_records:
                open_data_records[csw_id] = {}
            titles = get_element_value(element_record, 'title')
            if 'EN' in titles and titles['EN']:
                open_data_records[csw_id]['langs'] = ['EN']
            if 'FR' in titles and titles['FR']:
                open_data_records[csw_id]['langs'].append('FR')
            title = get_first_value(element_record, 'title')
            open_data_records[csw_id]['date'] = date
            open_data_records[csw_id]['title'] = title
            open_data_records[csw_id]['credits'] = credit_list
    return open_data_records


def get_preferred_service_types(element_tree):
    """
    Get the preferred service types (ESRI REST, WMS, etc.).

    :param element_tree
    :returns: service type string or None

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> print(get_preferred_service_types(et)[0])
    ESRI REST
    """
    service_types_by_lang, _ = get_service_types_and_urls(element_tree)
    if not service_types_by_lang['EN'] and not service_types_by_lang['FR']:
        return None
    # create an ordered list of preferred service types,
    # then return the first one
    types_found = [
        preferred for preferred in PREFERRED_TYPES
        if preferred in (
            service_types_by_lang['EN'] + service_types_by_lang['FR'])]
    if not types_found:
        return None
    return types_found


def get_elements_to_remove(element_ids, element_ids_in_agol):
    """
    Get a list of ArcGIS Online ids to remove.

    :param element_ids: list of existing CSW ids
    :param element_ids_in_agol: list of CSW ids found in AGOL items
    :return: a list of ArcGIS Online ids to remove

    >>> get_elements_to_remove(['a', 'b', 'c'], ['a', 'b', 'c', 'd'])
    ['d']
    """
    ids_to_remove = [
        agol_id for agol_id in element_ids_in_agol
        if agol_id not in element_ids]
    return ids_to_remove


def get_element_value(element_tree, tagname):
    """
    Get element value from a metadata tag name.

    :param element_tree: element tree from an XML document
    :param tagname: name of the XML tag

    :return: dictionary with a list of values by language
    {'EN': [<value>, ...], 'FR': [<value>, ...]}

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> values_by_lang = get_element_value(et, 'title')
    >>> values_by_lang['EN']
    ['Crop Development Stage']
    >>> values_by_lang['FR']
    ['Stade de développement de la culture']
    """
    values_by_lang = {'EN': [], 'FR': []}
    elements = {}
    for tag_location in ELEMENT_TAGS[tagname]:
        elements = element_tree.findall(tag_location, NAMESPACES)
        if elements:
            break
    if not elements:
        return {}
    for element in elements:
        if not element.text:
            continue
        value = element.text.strip()
        element_characterstring = element.find(
            '.gco:CharacterString', NAMESPACES)
        # check is the element is None since using
        # 'not element_characterstring' returns True if it's empty
        if element_characterstring is None:
            if value:
                values_by_lang['EN'].append(value)
                values_by_lang['FR'].append(value)
            continue
        if element_characterstring.text is None:
            values_by_lang['EN'].append('')
        else:
            values_by_lang['EN'].append(element_characterstring.text.strip())
        element_localized = element.find(
            '.gmd:PT_FreeText/.gmd:textGroup/.gmd:LocalisedCharacterString',
            NAMESPACES)
        if element_localized is None:
            continue
        elif 'locale' not in element_localized.attrib:
            continue
        elif element_localized.attrib['locale'] != '#fra':
            continue
        else:
            if element_characterstring.text is None:
                values_by_lang['FR'].append('')
            else:
                values_by_lang['FR'].append(element_localized.text.strip())
    if values_by_lang['EN'] and not values_by_lang['FR']:
        values_by_lang['FR'] = values_by_lang['EN']
    return values_by_lang


def get_extents(element_tree):
    """
    Get spatial extent values from a metadata element.

    :param element_tree: element tree from an XML document
    :return: list of extents (strings, west, south, east, north)

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> get_extents(et)[0]
    '-141.003'
    """
    element_extent = element_tree.find(ELEMENT_TAGS['extent'][0], NAMESPACES)
    if element_extent is None:
        return ['', '', '', '']
    extents = [
        element_extent.find(
            '.gmd:westBoundLongitude/.gco:Decimal', NAMESPACES).text,
        element_extent.find(
            '.gmd:southBoundLatitude/.gco:Decimal', NAMESPACES).text,
        element_extent.find(
            '.gmd:eastBoundLongitude/.gco:Decimal', NAMESPACES).text,
        element_extent.find(
            '.gmd:northBoundLatitude/.gco:Decimal', NAMESPACES).text]
    return extents


def get_service_types_and_urls(element_tree):
    """
    Get service types and associated urls.

    :param element_tree: element tree from an XML document
    :return: two dictionaries (types and urls) of lists by language

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> types_by_lang, urls_by_lang = get_service_types_and_urls(et)
    >>> 'WMS' in types_by_lang['EN']
    True
    >>> urls_by_lang['EN'][0].startswith('http')
    True
    """
    types_by_lang = {'EN': [], 'FR': []}
    urls_by_lang = {'EN': [], 'FR': []}
    types_by_lang_temp = {'EN': [], 'FR': []}
    urls_by_lang_temp = {'EN': [], 'FR': []}
    elements = element_tree.findall(ELEMENT_TAGS['online'][0], NAMESPACES)

    for element in elements:
        langs = []
        # first search if this is an language-specific resource
        # (some resources don't set this)
        if (
                element.attrib and
                '{http://www.w3.org/1999/xmlink}role' in element.attrib):
            role = element.attrib['{http://www.w3.org/1999/xmlink}role']
            if 'lang:eng' in role:
                langs.append('EN')
            elif 'lang:fra' in role:
                langs.append('FR')
        element_desc = element.find(
            '.gmd:CI_OnlineResource/.gmd:description/.gco:CharacterString',
            NAMESPACES)
        service_type = url = None
        if element_desc is not None and element_desc.text:
            _, service_type, lang = element_desc.text.split(';')
            if not langs:
                # lang can be 'eng', 'fra', 'eng,fra' or 'zxx' (no language)
                if 'eng' in lang.lower() or 'zxx' in lang.lower():
                    langs.append('EN')
                if 'fra' in lang.lower() or 'zxx' in lang.lower():
                    langs.append('FR')
        tag_desc2 = (
            '.gmd:CI_OnlineResource/.gmd:description/.gmd:PT_FreeText/' +
            '.gmd:textGroup/.gmd:LocalisedCharacterString')
        element_desc2 = element.find(tag_desc2, NAMESPACES)
        if (
                element_desc2 is not None and
                element_desc2.attrib and 'locale' in element_desc2.attrib):
            if not langs:
                if element_desc2.attrib['locale'].lower().startswith('#eng'):
                    langs.append('EN')
                if element_desc2.attrib['locale'].lower().startswith('#fra'):
                    langs.append('FR')
        else:
            pass
        element_url = element.find(
            '.gmd:CI_OnlineResource/.gmd:linkage/.gmd:URL', NAMESPACES)
        if element_url is not None and element_url.text:
            url = element_url.text.strip()
        for lang in langs:
            types_by_lang_temp[lang].append(service_type)
            urls_by_lang_temp[lang].append(url)
    for lang in ['EN', 'FR']:
        other_types = []
        other_urls = []
        other_complete = False
        for preferred_type in PREFERRED_TYPES:
            for i, type_check in enumerate(types_by_lang_temp[lang]):
                if not other_complete and type_check not in PREFERRED_TYPES:
                    other_types.append(type_check)
                    other_urls.append(urls_by_lang_temp[lang][i])
                    continue
                if type_check == preferred_type:
                    types_by_lang[lang].append(type_check)
                    urls_by_lang[lang].append(urls_by_lang_temp[lang][i])
            other_complete = True
        types_by_lang[lang] += other_types
        urls_by_lang[lang] += other_urls

    '''
    for lang in ['EN', 'FR']:
        lang2 = 'FR' if lang == 'EN' else 'EN'
        if not types_by_lang[lang]:
            types_by_lang[lang] = types_by_lang[lang2]
        if not urls_by_lang[lang]:
            urls_by_lang[lang] = urls_by_lang[lang2]
    # if it only finds a preferred type for one language,
    # use it for both languages
    if len(types_by_lang['EN']) != len(types_by_lang['FR']):
        for lang in ['EN', 'FR']:
            lang2 = 'FR' if lang == 'EN' else 'EN'
            types_found = [
                preferred for preferred in PREFERRED_TYPES
                if preferred in types_by_lang[lang]]
            if types_found:
                continue
            types_by_lang[lang] = types_by_lang[lang2]
            urls_by_lang[lang] = urls_by_lang[lang2]
    '''
    return types_by_lang, urls_by_lang


def get_source_acronyms(source_credits, acronyms_by_name):
    """
    Get the government acronyms of the source provider (e.g. AAFC).

    :param source_credits: metadata credits (accessInformation) value
    :param acronyms: dictionary of acronyms ('EN' and 'FR') by name
    :return: dictionary of acronymns by language
    >>> get_source_acronyms(
    ...    'Agence spatiale canadienne', ACRONYMS_BY_NAME)['EN']
    'CSA'
    >>> get_source_acronyms(
    ...    'Agence spatiale canadienne', ACRONYMS_BY_NAME)['FR']
    'ASC'
    """
    credit_list = [
        credit.strip()
        for credit in source_credits.split(';')]
    goc_found = False
    source_acronyms = None
    for credit in credit_list:
        if credit in [
                'Government of Canada', 'Gouvernement du Canada']:
            goc_found = True
            continue
        matches = [
            source for source in acronyms_by_name
            if source.lower() == credit.lower()]
        if matches:
            if len(matches) > 1:
                LOGGER.warning(
                    '*** Multiple source acronyms matches for %s ', credit)
                LOGGER.warning('*** Using the first match found.')
            source_acronyms = acronyms_by_name[matches[0]]
            break

    if not source_acronyms:
        if goc_found:
            source_acronyms = {'EN': 'GC', 'FR': 'GC'}
        else:
            source_acronyms = {'EN': 'UNKNOWN', 'FR': 'UNKNOWN'}
    return source_acronyms


def update_licence(licence, lang):
    """
    Fix and update the Open Data licence agreement.

    Changes HTTP to HTTPS and adds a hyperlinks to the URL.

    :param licence: licence text
    :param lang: language of licenses (EN or FR)
    :return: updated licence text

    >>> update_licence('No licence', 'EN')
    'No licence'
    >>> updated = update_licence('Test: ' + OPENDATA_LICENCE_URLS['EN'], 'EN')
    >>> updated.startswith('Test: <a href="' + OPENDATA_LICENCE_URLS['EN'])
    True
    """
    https_url = OPENDATA_LICENCE_URLS[lang]
    http_url = https_url.replace('https', 'http')
    if https_url not in licence and http_url not in licence:
        return licence
    if '<a href=' in licence:
        return licence
    licence = licence.replace(
        https_url,
        '<a href="' + https_url + '" target="_blank">' + https_url + '</a>')
    licence = licence.replace(
        http_url,
        '<a href="' + https_url + '" target="_blank">' + https_url + '</a>')
    return licence


def url_is_valid(url):
    """
    Test if a URL is valid.

    :param url: URL to test

    >>> url_is_valid('https://not_valid.not_valid')
    False
    >>> url_is_valid('https://canada.ca')
    True
    """
    try:
        urllib.request.urlopen(url)
        return True
    except urllib.error.HTTPError as e:
        # sometimes a 403 is a valid URL but call it False anyway
        if e.getcode() in [403]:
            return False
        LOGGER.debug('...... failed URL: %s', url)
        return False
    except Exception:
        LOGGER.debug('...... failed URL: %s', url)
        return False


def get_thumbnails(element_tree):
    """
    Get the small and large thumbnails, if they exist.

    :param element_tree: element tree from an XML document
    :return: dictionary of thumbnail URLs by language

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> thumbs = get_thumbnails(et)
    >>> thumbs['FR']['SMALL'].endswith('90071_s_fre.png')
    True
    >>> thumbs['EN']['SMALL'].endswith('90071_s.png')
    True
    """
    formats = ['jpg', 'jpeg', 'tif', 'gif', 'png']
    thumbnails = {'EN': {}, 'FR': {}}
    # thumbnails = {
    #     'EN': {'SMALL': '', 'LARGE': ''},
    #     'FR': {'SMALL': '', 'LARGE': ''}}
    elements = element_tree.findall(ELEMENT_TAGS['thumbnails'][0], NAMESPACES)
    thumbnail_url = None
    for element in elements:
        element_desc = element.find(
            '.gmd:fileDescription/.gco:CharacterString', NAMESPACES)
        desc = None
        if element_desc is not None:
            desc = element_desc.text
        if element_desc is None or not desc:
            desc = 'large_thumbnail'
        LOGGER.debug('... thumbnail desc: %s', desc)
        element_filename = element.find(
            '.gmd:fileName/.gco:CharacterString', NAMESPACES)
        filename = None
        if element_filename is not None and element_filename.text:
            filename = element_filename.text.strip('"')
        if element_filename is None or not filename:
            continue
        if not filename.split('.')[-1].lower() in formats:
            LOGGER.info('... %s is not a valid thumbnail url', filename)
            continue
        url = None
        if filename.lower().startswith('http'):
            url = filename
        if not url or not url_is_valid(url):
            continue
        if desc in ['thumbnail', 'thumbnail_eng']:
            thumbnails['EN']['SMALL'] = url
        elif desc == 'thumbnail_fre':
            thumbnails['FR']['SMALL'] = url
        elif desc == 'large_thumbnail':
            thumbnails['EN']['LARGE'] = url
            if 'LARGE' not in thumbnails['FR']:
                thumbnails['FR']['LARGE'] = url
        elif desc == 'large_thumbnail_fre':
            thumbnails['FR']['LARGE'] = url
        else:
            thumbnails['EN']['LARGE'] = url
            thumbnails['FR']['LARGE'] = url
        thumbnail_url = url
        LOGGER.debug('... thumbnail: %s', thumbnail_url)
    if thumbnail_url and not thumbnails['EN'] and not thumbnails['FR']:
        thumbnails['EN']['LARGE'] = thumbnail_url
        thumbnails['FR']['LARGE'] = thumbnail_url
    return thumbnails


def get_metadata(element_tree):
    """
    Parse metadata from a CSW element tree.

    Creates a dictionary that can used to add a new item to ArcGIS Online.
    Optional key:value pairs that will be populated:
        type: 'Map Service' (everything added here will be a Map Service)
        dataUrl: N/A
        filename: N/A
        typeKeywords: N/A
        description : string
        title: string
        url: string
        text: N/A
        tags: comma-delimited string or list
        snippet: string
        extent: comma-delimited string
        spatialReference: string (EPSG:<code>)
        accessInformation: string
        licenseInfo: string
        culture: N/A
        commentsEnabled: true
        overwrite: false unless explicitly set later (when updating)

    :param element_tree: element tree from an XML document
    :return: dictionary of metadata values by language and metadata tag

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> metadata_by_lang = get_metadata(et)
    >>> 'Crops' in metadata_by_lang['EN']['tags'].split(',')
    True
    >>> metadata_by_lang['FR']['title']
    'Stade de développement de la culture'
    """
    metadata_by_lang = {'EN': {}, 'FR': {}}
    service_types, urls = get_service_types_and_urls(element_tree)
    if not service_types:
        return None
    '''
    if 'EN' not in service_types or not service_types['EN']:
        service_types['EN'] = service_types['FR']
        urls['EN'] = urls['FR']
    elif 'FR' not in service_types or not service_types['FR']:
        service_types['FR'] = service_types['EN']
        urls['FR'] = urls['EN']
    '''
    langs = ['EN', 'FR']
    # note service_types is already orderd based on PREFERRED_TYPES
    for lang in langs:
        for i, service_type in enumerate(service_types[lang]):
            if service_type in PREFERRED_TYPES:
                metadata_by_lang[lang]['url'] = urls[lang][i]
                break
        # if 'url' not in metadata_by_lang[lang] or not metadata_by_lang[lang]['url']:
        #     langs.remove(lang)
        #     continue
    if 'url' not in metadata_by_lang['EN']:
        metadata_by_lang['EN']['url'] = metadata_by_lang['FR']['url']
    elif 'url' not in metadata_by_lang['FR']:
        metadata_by_lang['FR']['url'] = metadata_by_lang['EN']['url']

    metadata_date = get_element_value(element_tree, 'date')
    guids = get_element_value(element_tree, 'guid')
    spatial_reference = get_element_value(element_tree, 'spatialReference')
    extents = get_extents(element_tree)

    titles = get_element_value(element_tree, 'title')
    descriptions = get_element_value(element_tree, 'description')

    snippets = get_element_value(element_tree, 'snippet')
    if not snippets:
        snippets = titles.copy()

    licence_infos = get_element_value(element_tree, 'licenceInfo')
    tags = get_element_value(element_tree, 'tags')

    source_credits = get_element_value(element_tree, 'credits')
    thumbnails = get_thumbnails(element_tree)

    for lang in langs:
        metadata_by_lang[lang]['date'] = metadata_date[lang][0]
        metadata_by_lang[lang]['guid'] = guids[lang][0]
        metadata_by_lang[lang]['spatialReference'] = spatial_reference[lang][0]
        metadata_by_lang[lang]['extent'] = ','.join(extents)
        metadata_by_lang[lang]['type'] = 'Map Service'
        if len(titles[lang][0]) > 255:
            LOGGER.error(
                '... %s title is too long, reducing to 256 chararcters', lang)
            titles[lang][0] = titles[lang][0][:256]
        metadata_by_lang[lang]['title'] = titles[lang][0]
        metadata_by_lang[lang]['description'] = update_description(
            descriptions[lang][0], guids[lang][0], lang)
        metadata_by_lang[lang]['snippet'] = snippets[lang][0]
        if not snippets[lang][0]:
            metadata_by_lang[lang]['snippet'] = titles[lang][0]
        if len(snippets[lang][0]) > 2047:
            if (
                    snippets[lang][0] != titles[lang][0] and
                    len(titles[lang][0]) < 2047):
                metadata_by_lang[lang]['snippet'] = titles[lang][0]
            else:
                LOGGER.error('... snippet is too long.')
                return None
        metadata_by_lang[lang]['accessInformation'] = source_credits[lang][0]
        metadata_by_lang[lang]['licenseInfo'] = update_licence(
            licence_infos[lang][0], lang)
        # tags will become a comma-delimited string, so replace any commas
        # in a single tag with a semi-colon
        for i, tag in enumerate(tags[lang][:]):
            if ',' in tag or len(tag) > 255:
                tags[lang].remove(tag)
        if len(tags[lang]) > 126:
            LOGGER.info('... too many %s tags, reducing...', lang)
            tags[lang] = tags[lang][:126]
        metadata_by_lang[lang]['tags'] = ','.join(
            tags[lang] + ['lang-' + lang.lower(), guids[lang][0]])
        if 'LARGE' in thumbnails[lang]:
            metadata_by_lang[lang]['thumbnailUrl'] = thumbnails[lang]['LARGE']
        elif 'SMALL' in thumbnails[lang]:
            metadata_by_lang[lang]['thumbnailUrl'] = thumbnails[lang]['SMALL']
        else:
            metadata_by_lang[lang]['thumbnailUrl'] = None

    return metadata_by_lang


def delete_agol_items_based_on_csw_id(csw_ids, agol_items_by_csw_id_lang):
    """
    Delete ArcGIS Online items associated with specified CSW items.

    :param csw_ids: list of CSW (Open Data) unique identifiers
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: True if items were deleted

    >>> item = create_test_item()
    >>> csw_ids = ['test_csw_id']
    >>> agol_items = {'test_csw_id': {'EN': {'id': item.id}}}
    >>> delete_agol_items_based_on_csw_id(csw_ids, agol_items)
    True
    """
    items = GIS.content.search(query='owner:' + USERNAME, max_items=10000)
    agol_ids_to_delete = []
    for csw_id in agol_items_by_csw_id_lang:
        if csw_id not in csw_ids:
            continue
        try:
            for lang in agol_items_by_csw_id_lang[csw_id]:
                if (
                        not agol_items_by_csw_id_lang[csw_id][lang] or
                        'id' not in agol_items_by_csw_id_lang[csw_id][lang]):
                    continue
                agol_ids_to_delete.append(
                    agol_items_by_csw_id_lang[csw_id][lang]['id'])
        except TypeError:
            pass
    items_deleted = False
    for item in items:
        agol_id = item.id
        if agol_id not in agol_ids_to_delete:
            continue
        LOGGER.info('... deleting AGOL item %s', agol_id)
        if not item.can_delete:
            item.protect(enable=False)
        result = item.delete()
        if not result:
            LOGGER.warning('... could not delete %s', agol_id)
        else:
            items_deleted = True
    if not items_deleted:
        return False
    return True


def get_agol_items(folder_names=None):
    """
    Get a dictionary of ArcGIS Online items, using the CSW id as the key.

    :param folder_names: list of ArcGIS Online folder names to search
    :return: ArcGIS Online item objects by CSW id and language
    >>> items = get_agol_items()
    >>> 'Crops' in items[TEST_OPENDATA_URL.split('=')[-1]]['EN']['tags']
    True
    """
    agol_items_by_csw_id_lang = {}
    items = GIS.content.search(query='owner:' + USERNAME, max_items=10000)
    agol_ids_by_csw_id_lang = {}
    urls_duplicates = []
    folder_ids = []
    if folder_names:
        for folder_name in folder_names:
            for folder in GIS.users.me.folders:
                if folder['title'] == folder_name:
                    folder_ids.append(folder['id'])
    for item in items:
        folder_id = item.ownerFolder

        if folder_names and folder_id not in folder_ids:
            continue
        agol_id = item.id
        csw_ids = get_uids_in_text(str(item.tags), hyphens=True)
        if not csw_ids:
            continue
        if len(csw_ids) > 1:
            LOGGER.error('... multiple CSW uid values in tags')
            LOGGER.error('... fix and then run again.')
            webbrowser.open(AGOL_ITEM_URL + agol_id)
            return None
        csw_id = csw_ids[0]
        # English AGOL records have a tag "lang-en", and French ones
        # have "lang-fr".

        # There should be two AGOL records for each Open Data record:
        # one English and one French
        # In earlier years this script was building bilingual AGOL records,
        # but now we need to replace those with one English and one French.
        # If we find an older bilingual record, replace it with a new English
        # one and create a new separate French record.

        lang = 'EN'
        if 'lang-fr' in item.tags and 'lang-en' not in item.tags:
            lang = 'FR'
        if csw_id not in agol_ids_by_csw_id_lang:
            agol_ids_by_csw_id_lang[csw_id] = {'EN': None, 'FR': None}
        if (
                agol_ids_by_csw_id_lang[csw_id][lang] and
                agol_id != agol_ids_by_csw_id_lang[csw_id][lang]):
            urls_duplicates.append(AGOL_ITEM_URL + agol_id)
            urls_duplicates.append(
                AGOL_ITEM_URL + agol_ids_by_csw_id_lang[csw_id][lang])
        else:
            agol_ids_by_csw_id_lang[csw_id][lang] = agol_id
        if csw_id not in agol_items_by_csw_id_lang:
            agol_items_by_csw_id_lang[csw_id] = {'EN': None, 'FR': None}
        # ideally we'd just add the result of 'item.shared_with' to a
        # 'share' key but that will cause a permissions error if this item
        # is shared with an external group that isn't public
        '''
        group_ids = []
        for group in item.shared_with['groups']:
            try:
                group_ids.append(group.id)
            except Exception:
                # it's possible this item is shared with an external group
                # that we can't access, which will throw this exception
                continue
        '''
        agol_items_by_csw_id_lang[csw_id][lang] = {
            'id': agol_id,
            'title': item.title,
            'tags': item.tags,
            'categories': item.categories,
            'url': item.url,
            'credits': item.accessInformation,
            # 'groups': group_ids,
            'public': item.access == 'public',
            'categories': item.categories,
            # Esri timestamps are in milliseconds, we'll be comparing this
            # later with timestamps recorded as seconds
            'modified': item.modified / 1000}
    if urls_duplicates:
        LOGGER.error(
            '... %s duplicate items found in AGOL', len(urls_duplicates))
        LOGGER.error('... remove duplicates before continuing')
        for url in urls_duplicates:
            webbrowser.open(url)
            time.sleep(2)
        return None
    return agol_items_by_csw_id_lang


def get_new_csw_ids(csw_ids, agol_items_by_csw_id_lang):
    """
    Get a list of CSW ids that have no corresponding ArcGIS Online records.

    :param csw_ids: list of CSW (Open Data) unique identifiers
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: dictionary of CSW ids, each value a list of languages to create

    >>> csw_ids = ['a', 'b', 'c']
    >>> agol_items = {'a': {'EN': {'id': 'aa'}}}
    >>> sorted(list(get_new_csw_ids(csw_ids, agol_items).keys()))
    ['b', 'c']
    """
    new_csw_ids = {}
    for csw_id in csw_ids:
        if csw_id not in agol_items_by_csw_id_lang:
            new_csw_ids[csw_id] = ['EN', 'FR']
            continue
        for lang in ['EN', 'FR']:
            if lang not in agol_items_by_csw_id_lang[csw_id]:
                continue
            if not agol_items_by_csw_id_lang[csw_id][lang]:
                new_csw_ids[csw_id] = [lang]
    return new_csw_ids


def get_removed_csw_ids(csw_ids, agol_items_by_csw_id_lang):
    """
    Get a list of CSW ids in AGOL that no longer exist in Open Data.

    :param csw_ids: list of CSW (Open Data) unique identifiers
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: list of AGOL items to remove

    >>> csw_ids = ['a', 'c']
    >>> agol_items = {'a': {'EN': {'id': 'aa'}}, 'b': {'EN': {'id': 'ab'}}}
    >>> get_removed_csw_ids(csw_ids, agol_items)
    ['b']
    """
    agol_csw_ids = set(list(agol_items_by_csw_id_lang.keys()))
    return list(agol_csw_ids.difference(set(csw_ids)))


def get_existing_csw_ids(csw_ids, agol_items_by_csw_id_lang):
    """
    Get a list of CSW ids that already exist in ArcGIS Online.

    :param csw_ids: list of CSW (Open Data) unique identifiers
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: list of CSW ids found in AGOL items

    >>> csw_ids = ['a', 'c']
    >>> agol_items = {'a': {'EN': {'id': 'aa'}}, 'b': {'EN': {'id': 'ab'}}}
    >>> get_existing_csw_ids(csw_ids, agol_items)
    ['a']
    """
    agol_csw_ids = set(list(agol_items_by_csw_id_lang.keys()))
    return list(set(csw_ids).intersection(agol_csw_ids))


def get_csw_ids_to_update(
        existing_csw_ids_in_agol,
        open_data_records,
        agol_items_by_csw_id_lang):
    """
    Get a list of CSW ids for records that need to be updated.

    :param existing_csw_ids_in_agol: list of existing CSW ids in AGOL
    :param open_data_records: dictionary of the CSW item modified unix date
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: list of CSW ids in AGOL items that have updated metadata

    >>> existing = ['a', 'b', 'c', 'd']
    >>> dates_csw = {
    ...     'a': {'date': 1000}, 'b': {'date': 2000},
    ...     'c': {'date': 1000}, 'd': {'date': 1000}}
    >>> agol_items = {
    ...     'a': {'EN': {'modified': 1000}, 'FR': {'modified': 1000}},
    ...     'b': {'EN': {'modified': 1000}, 'FR': {'modified': 1000}},
    ...     'c': {'EN': {'modified': 1000}, 'FR': {'modified': 500}},
    ...     'd': {'EN': {'modified': 1000}}}
    >>> get_csw_ids_to_update(existing, dates_csw, agol_items)['EN']
    ['b']
    >>> sorted(get_csw_ids_to_update(existing, dates_csw, agol_items)['FR'])
    ['b', 'c']
    """
    csw_ids_to_update_by_lang = {'EN': [], 'FR': []}
    for csw_id in existing_csw_ids_in_agol:
        for lang in ['EN', 'FR']:
            if lang not in agol_items_by_csw_id_lang[csw_id]:
                continue
            if (
                agol_items_by_csw_id_lang[csw_id][lang] and (
                    open_data_records[csw_id]['date'] >
                    agol_items_by_csw_id_lang[csw_id][lang]['modified'])):
                csw_ids_to_update_by_lang[lang].append(csw_id)

    return csw_ids_to_update_by_lang


def update_agol_item(agol_id, item_properties, thumbnail=None):
    """
    Update an existing ArcGIS Online item.
    """
    try:
        item = arcgis.gis.Item(GIS, agol_id)
        result = item.update(
            item_properties=item_properties,
            thumbnail=item_properties['thumbnailUrl'])
        if not result:
            LOGGER.error('... failed updating item (lang=%s)', lang)
            return None
        return item
    except urllib.error.HTTPError:
        LOGGER.error('... failed updating item due to HTTP error')
        LOGGER.error('... likely due to a forbidden thumbnail url')
        LOGGER.error('', exc_info=True)
    except urllib.error.URLError:
        LOGGER.error('... failed getting item info', exc_info=True)
    except Exception:
        LOGGER.error('... exception updating item', exc_info=True)
    return None


def update_agol_items_from_csw_info(
        csw_ids_to_update_by_lang, agol_items_by_csw_id_lang):
    """
    Update existing ArcGIS Online items with new metadata.

    :param csw_ids_to_update_by_lang: dictionary of CSW items by language
    :param agol_items_by_csw_id_lang: dictionary of agol items by CSW
    :return: True if successful
    """
    i = 0
    record_num = (
        len(csw_ids_to_update_by_lang['EN']) +
        len(csw_ids_to_update_by_lang['FR']))
    no_items_updated = True
    for lang in csw_ids_to_update_by_lang:
        for csw_id in csw_ids_to_update_by_lang[lang]:
            i += 1
            LOGGER.info('%s of %s: %s (%s)', i, record_num, csw_id, lang)
            element_tree = get_full_csw_record(csw_id)
            metadata_by_lang = get_metadata(element_tree)
            if not metadata_by_lang:
                LOGGER.error('... could not get metadata')
                continue
            if not metadata_by_lang[lang]:
                LOGGER.info('... no %s metadata', lang)
                continue
            if csw_id not in agol_items_by_csw_id_lang:
                continue
            source_acronym = get_source_acronyms(
                metadata_by_lang[lang]['accessInformation'],
                ACRONYMS_BY_NAME)[lang]
            if source_acronym:
                LOGGER.info(
                    '%s (%s)', metadata_by_lang[lang]['title'], source_acronym)
            else:
                LOGGER.info(metadata_by_lang[lang]['title'])
            '''
            orgs = [
                ACRONYMS_BY_NAME[org.strip()][lang]
                for org in metadata_by_lang[lang]['accessInformation'].split(';')
                if org.strip() in ACRONYMS_BY_NAME]
            if orgs:
                LOGGER.info(
                    '%s (%s)', metadata_by_lang[lang]['title'], orgs[0])
            else:
                LOGGER.info(metadata_by_lang[lang]['title'])
            '''
            agol_item = agol_items_by_csw_id_lang[csw_id][lang]
            if LOGGER.level == logging.DEBUG:
                webbrowser.open(AGOL_ITEM_URL + agol_item['id'])
            if not agol_item:
                continue
            item_properties = metadata_by_lang[lang]
            item = update_agol_item(
                agol_item['id'], item_properties,
                thumbnail=item_properties['thumbnailUrl'])
            if not item:
                LOGGER.error('... failed updating item')
                continue
            categories_by_lang = get_categories(element_tree)

            result = assign_categories(
                agol_item['id'], categories_by_lang[lang])
            if not result:
                LOGGER.error('... failed assigning categories')
            group_ids = get_required_item_group_ids(
                item_properties['accessInformation'], lang)
            result = item.share(everyone=True, groups=group_ids)
            no_items_updated = False
            if LOGGER.level == logging.DEBUG:
                webbrowser.open(AGOL_ITEM_URL + agol_item['id'])
    if no_items_updated:
        return False
    return True


def update_description(description, csw_id, lang):
    """
    Update the description of an item to include required links.

    :param description: text description
    :param csw_id: CSW (Open Data) unique identifier
    :param lang: language (EN or FR)
    :return: an updated description

    >>> updated = update_description('No licence', 'aaa', 'EN')
    >>> 'For more information' in updated and 'aaa' in updated
    True
    >>> OPENDATA_ITEM_URL['EN'] in updated
    True
    """
    if csw_id in description:
        return description
    if lang == 'EN':
        description += (
            '<br/><br/>For more information, visit: ' +
            '<a href="{}{}'.format(OPENDATA_ITEM_URL['EN'], csw_id) +
            '" target="_blank">' +
            '{}{}</a>'.format(OPENDATA_ITEM_URL['EN'], csw_id))
    else:
        description += (
            '<br/><br/>Pour plus d\'information, consulter : ' +
            '<a href="{}{}'.format(OPENDATA_ITEM_URL['FR'], csw_id) +
            '" target="_blank">' +
            '{}{}</a>'.format(OPENDATA_ITEM_URL['FR'], csw_id))
    return description


def get_categories(element_tree):
    """
    Determine the appropriate ArcGIS categories for this AGOL item.

    :param element_tree: element tree from an XML document
    :return: dictionary containing lists of categories by language
    {
        'EN': [<category 1>, <category 2>, ...],
        'FR': [<category 1>, <category 2>, ...]}

    >>> et = get_element_tree(TEST_OPENDATA_URL)
    >>> categories = get_categories(et)
    >>> categories['EN']
    ['/Categories/Farming']
    >>> categories['FR']
    ['/Categories/Agriculture']
    """
    categories_by_lang = get_element_value(element_tree, 'categories')
    if (
            not categories_by_lang or
            (
                'EN' not in categories_by_lang and
                'FR' not in categories_by_lang)):
        return {'EN': [], 'FR': []}
    for lang in ['EN', 'FR']:
        for i, category in enumerate(categories_by_lang[lang]):
            categories_by_lang[lang][i] = \
                '/Categories/' + CATEGORIES[category][lang]

    return categories_by_lang


def assign_categories(agol_id, categories):
    """
    Determine the appropriate ArcGIS categories for this AGOL item.

    :param agol_id: ArcGIS Online item identifier
    :param categories: list of categories suitable for updating an AGOL item
    :return: True if successful, else False
    """
    item_categories = {agol_id: {'categories': []}}
    for category in categories:
        item_categories[agol_id]['categories'].append(category)
    result = GIS.content.categories.assign_to_items([item_categories])
    if 'success' in result[0]:
        return result[0]['success']
    return False


def share_item(item, group_ids=None):
    """
    Share an item with relevant groups and the public.

    :param item: an ArcGIS Online item object
    :param group_ids: list of AGOL group identifiers to share with
    :return: True if successful, else False

    >>> item = create_test_item()
    >>> share_item(item, ['ec193ab4b886484692e07e9b02740156'])
    True
    >>> item = arcgis.gis.Item(GIS, item.id)
    >>> 'ec193ab4b886484692e07e9b02740156' in item.shared_with['groups'][0].id
    True
    >>> item.shared_with['everyone']
    True
    >>> delete_agol_item(item.id)
    True
    """
    result = item.share(everyone=True, groups=group_ids)
    if not result['results'][0]['success'] or not item.shared_with['everyone']:
        return False
    return True


def get_required_item_group_ids(source_credits, lang):
    """
    Get a list of ArcGIS Online group ids associated with credits values.

    :param source_credits: metadata credits (accessInformation) value
    :param lang: language (EN or FR)
    :return: list of ArcGIS Online group identifiers

    >>> get_required_item_group_ids('Agriculture and Agri-Food Canada', 'EN')
    ['ec193ab4b886484692e07e9b02740156']
    >>> get_required_item_group_ids('non-existent group', 'EN')
    []
    >>> get_required_item_group_ids(
    ...     'Agriculture et Agroalimentaire Canada', 'FR')
    ['c101f867dbef49c2b1a70a8ac90f2e10']
    """
    global GIS
    required_group_ids = []
    group_ids_by_name = {}
    groups = GIS.users.me.groups
    for group in groups:
        group_ids_by_name[group.title] = group.id
    credit_list = [
        credit.strip()
        for credit in source_credits.split(';')]
    credit_list = list(set(credit_list))
    for credit in credit_list:
        if credit in group_ids_by_name:
            required_group_ids.append(group_ids_by_name[credit])
            continue
        if credit in ACRONYMS_BY_NAME:
            # create a new group if the org name (credit) is valid but
            # doesn't exist
            LOGGER.info('... creating group "%s"', credit)
            group = GIS.groups.create(
                title=credit,
                tags='Canada,lang-{}'.format(lang.lower()),
                description=GROUP_DESCRIPTIONS[lang] + ' ' + credit + '.',
                snippet=GROUP_DESCRIPTIONS[lang] + ' ' + credit + '.',
                access='org')
            if group:
                required_group_ids.append(group.id)
                # pausing to allow the group creation to finish, otherwise
                # it won't be found when attempting to share to it
                time.sleep(5)
                # update the user object with the new group
                GIS.users.me._hydrate()
                # group_ids_by_name[group.title] = group.id

    if not required_group_ids:
        LOGGER.warning('No groups exist for this item')
        LOGGER.warning('Create a group with one of these titles:')
        for credit in credit_list:
            LOGGER.warning('--- %s', credit)

    # for group in groups:
    #     group_ids_by_name[group.title] = group.id
    #     if group.title in ['Government of Canada', 'Gouvernement du Canada']:
    #         required_group_ids.append(group.id)
    return list(set(required_group_ids))


def add_csw_item_to_agol(csw_id, lang=None):
    """
    Add an Open Data item to ArcGIS Online.

    Typically this will add one English AGOL item and one French.
    :param csw_id: CSW (Open Data) unique identifier
    :param folder_names: list of folder names in AGOL account
    :returns: AGOL item indentifier dictionary by language

    >>> csw_id = TEST_OPENDATA_URL.split('=')[-1]
    >>> agol_ids_by_lang = add_csw_item_to_agol(csw_id)
    >>> agol_ids_by_lang['EN'] is not None
    True
    >>> delete_agol_item(agol_ids_by_lang['EN'])
    True
    >>> delete_agol_item(agol_ids_by_lang['FR'])
    True
    """
    folder_names = []
    user = GIS.users.me
    folder_names = [folder_info['title'] for folder_info in user.folders]
    element_tree = get_full_csw_record(csw_id)

    metadata_by_lang = get_metadata(element_tree)
    if not metadata_by_lang:
        LOGGER.error('... failed to get Open Data metadata')
        return None
    if lang:
        for metadata_lang in metadata_by_lang.copy():
            if metadata_lang != lang:
                del metadata_by_lang[metadata_lang]
    categories_by_lang = get_categories(element_tree)
    if not metadata_by_lang:
        LOGGER.error('... failed to add record.')
        return None
    agol_ids_by_lang = {'EN': None, 'FR': None}
    for lang in metadata_by_lang:
        if (
                'url' not in metadata_by_lang[lang] or
                not metadata_by_lang[lang]['url']):
            LOGGER.warning('... no %s metadata, no url found', lang)
            continue
        LOGGER.debug('... %s', lang)
        item_properties = metadata_by_lang[lang]
        thumbnail = item_properties['thumbnailUrl']
        # Get the source acronym to use as a the folder name.
        # Use the English version for both languages to limit the
        # the number of folders in the account.
        source_acronym = get_source_acronyms(
            metadata_by_lang[lang]['accessInformation'],
            ACRONYMS_BY_NAME)[lang]
        LOGGER.info('%s (%s)', metadata_by_lang[lang]['title'], source_acronym)
        LOGGER.debug('... adding to AGOL')
        if source_acronym not in folder_names:
            GIS.content.create_folder(folder=source_acronym)
        try:
            item = GIS.content.add(
                item_properties=item_properties,
                thumbnail=thumbnail,
                folder=source_acronym)
            if not item:
                LOGGER.error('... failed adding new item')
                continue

            result = assign_categories(
                item.id, categories_by_lang[lang])
            if not result:
                LOGGER.error('... failed assigning categories')

            group_ids = get_required_item_group_ids(
                item_properties['accessInformation'], lang)
            result = item.share(everyone=True, groups=group_ids)
            if not result:
                LOGGER.error('... failed sharing item')
            agol_ids_by_lang[lang] = item.id

        except urllib.error.URLError:
            LOGGER.error('... failed adding new item', exc_info=True)
            return None
    return agol_ids_by_lang


def validate_agol_items(agol_items_by_csw_id_lang):
    """
    Validate existing ArcGIS Online items.

    Fixes categories, group, language, sharing.
    """
    for csw_id in agol_items_by_csw_id_lang:
        for lang in agol_items_by_csw_id_lang[csw_id]:
            agol_item = agol_items_by_csw_id_lang[csw_id][lang]
            agol_id = agol_item['id']
            title = agol_item['title']
            source_credits = agol_item['credits']
            source_lang = None
            item_properties = {}
            if not csw_id:
                LOGGER.info('... item %s has no CSW id in its tags', title)
            credit_list = [
                credit.strip() for credit in source_credits.split(';')]
            for credit in credit_list:
                for lang in ['EN', 'FR']:
                    if credit in CREDITS_BY_LANGUAGE[lang]:
                        source_lang = lang
            if not source_lang:
                LOGGER.warning(
                    '... no source found in the department dictionaries.')
                LOGGER.warning('... add one of the following to globals.py:')
                LOGGER.warning('... %s', source_credits)
                continue
            source_lang_tag = 'lang-' + source_lang.lower()
            lang_tag = None
            for tag in agol_item['tags']:
                if tag.startswith('lang-'):
                    lang_tag = tag
                    break
            if not lang_tag:
                LOGGER.info('... no language tag found in %s', title)
                agol_item['tags'].append(source_lang_tag)
                item_properties['tags'] = agol_item['tags']
            elif source_lang_tag not in agol_item['tags']:
                LOGGER.info(
                    '... wrong language tag (%s) found in %s', lang_tag, title)
                for i, tag in enumerate(agol_item['tags'][::]):
                    if tag.startswith('lang-'):
                        agol_item['tags'] = source_lang_tag
                item_properties['tags'] = agol_item['tags']
            not_shared = False
            if not agol_item['public']:
                LOGGER.info('... item %s is not public', title)
                not_shared = True
            if not_shared:
                LOGGER.info('... sharing item')
                item = arcgis.gis.Item(GIS, agol_id)
                result = share_item(item)
                if not result:
                    LOGGER.error('...... failed sharing item')
            item = None
            if item_properties:
                LOGGER.info('... updating item')
                item = update_agol_item(agol_id, item_properties)
            if not agol_item['categories']:
                LOGGER.info('... item %s has no assigned categories', title)
                element_tree = get_full_csw_record(csw_id)
                categories_by_lang = get_categories(element_tree)
                result = assign_categories(
                    agol_id, categories_by_lang[source_lang])
                if not result:
                    LOGGER.error('... failed assigning categories')
    return agol_items_by_csw_id_lang


def main():
    """Main function."""
    args = parse_args()
    if args.doctest:
        import doctest
        doctest.testmod()
        return True
    process_all = args.all
    status_only = args.status
    use_files = args.file
    org = args.org
    update_only = args.update
    validate = args.validate
    if args.debug:
        LOGGER.setLevel(logging.DEBUG)
    selected_folder_names = None
    delete_items = args.delete
    selected_org = None
    if not process_all:
        if not org:
            print('-----------------------------------------------')
            print('Enter department or organization name or acronym:')
            print('(Use \'list\' to get a listing, or <Enter> to process all)')
            print('-----------------------------------------------')
        orgs = sorted(list(ACRONYMS_BY_NAME.keys()))
        value = org
        while 1:
            partial_matches = []
            if not org:
                value = input('\nOrganization name: ')
                if not value:
                    process_all = True
                    break
            if value == 'list':
                for org in orgs:
                    print('{} ({} or {})'.format(
                        org,
                        ACRONYMS_BY_NAME[org]['EN'],
                        ACRONYMS_BY_NAME[org]['FR']))
                print()
            else:
                found = False
                for name in ACRONYMS_BY_NAME:
                    if (
                            value == ACRONYMS_BY_NAME[name]['EN'] or
                            value == ACRONYMS_BY_NAME[name]['FR']):
                        found = True
                        value = name
                    elif (
                            value in ACRONYMS_BY_NAME[name]['EN'] or
                            value in ACRONYMS_BY_NAME[name]['FR']):
                        partial_matches.append(name)
                    elif value in name:
                        partial_matches.append(name)
                if found:
                    LOGGER.info('Organization found: %s', value)
                    selected_org = value
                    selected_folder_names = [
                        ACRONYMS_BY_NAME[value]['EN'],
                        ACRONYMS_BY_NAME[value]['FR']]
                    break
            if partial_matches:
                print('\nOrganization not found. Partial matches:\n')
                for partial_match in partial_matches:
                    print('{} ({} or {})'.format(
                        partial_match,
                        ACRONYMS_BY_NAME[partial_match]['EN'],
                        ACRONYMS_BY_NAME[partial_match]['FR']))
                value = org = None
                continue
            print('Organization not found.')
            break
    if not process_all and not selected_org:
        return None
    LOGGER.info('Retrieving existing AGOL items by source organization...')
    agol_items_by_csw_id_lang = get_agol_items(
        folder_names=selected_folder_names)
    if agol_items_by_csw_id_lang is None:
        return None
    if not agol_items_by_csw_id_lang:
        if process_all:
            LOGGER.info('... none found')
        else:
            LOGGER.info('... none found for %s', selected_org)
    if agol_items_by_csw_id_lang and validate:
        LOGGER.info('... validating existing AGOL items...')
        validate_agol_items(agol_items_by_csw_id_lang)
    LOGGER.info('Retrieving all Open Data mappable service items ...')
    if os.path.exists(CSW_FILE) and use_files:
        open_data_records = pickle.load(open(CSW_FILE, 'rb'))
    else:
        open_data_records = get_open_data_records()
        if not open_data_records:
            LOGGER.error('... could not retrieve Open Data records')
            return None
        pickle.dump(open_data_records, open(CSW_FILE, 'wb'))

    if selected_org:
        # get English and French names to see if either are in the credits
        org_names = []
        for org_name in ACRONYMS_BY_NAME:
            if (
                    ACRONYMS_BY_NAME[org_name]['EN'] ==
                    ACRONYMS_BY_NAME[selected_org]['EN']):
                org_names.append(org_name)
        for csw_id in open_data_records.copy():
            if not set(org_names).intersection(
                    set(open_data_records[csw_id]['credits'])):
                del open_data_records[csw_id]

    csw_ids = list(open_data_records.keys())
    csw_items_count_en = len([
        csw_id for csw_id in open_data_records
        if 'EN' in open_data_records[csw_id]['langs']])
    csw_items_count_fr = len([
        csw_id for csw_id in open_data_records
        if 'FR' in open_data_records[csw_id]['langs']])
    agol_items_count_en = len([
        csw_id for csw_id in agol_items_by_csw_id_lang
        if agol_items_by_csw_id_lang[csw_id]['EN']])
    agol_items_count_fr = len([
        csw_id for csw_id in agol_items_by_csw_id_lang
        if agol_items_by_csw_id_lang[csw_id]['FR']])
    LOGGER.info(
        'Number of Open Data items in AGOL account: %s English, %s French',
        agol_items_count_en, agol_items_count_fr)
    LOGGER.info(
        'Number of mappable items in Open Data: %s English, %s French',
        csw_items_count_en, csw_items_count_fr)
    zero_count = 0
    new_csw_ids = get_new_csw_ids(csw_ids, agol_items_by_csw_id_lang)
    if not new_csw_ids:
        zero_count += 1
    new_csw_ids_en = len([
        csw_id for csw_id in new_csw_ids if 'EN' in new_csw_ids[csw_id]])
    new_csw_ids_fr = len([
        csw_id for csw_id in new_csw_ids if 'FR' in new_csw_ids[csw_id]])
    LOGGER.info(
        'Number of new Open Data items to add: %s English, %s French',
        new_csw_ids_en, new_csw_ids_fr)
    existing_csw_ids_in_agol = get_existing_csw_ids(
        csw_ids, agol_items_by_csw_id_lang)
    csw_ids_to_update_by_lang = get_csw_ids_to_update(
        existing_csw_ids_in_agol,
        open_data_records,
        agol_items_by_csw_id_lang)
    count_csw_ids_to_update = (
        len(csw_ids_to_update_by_lang['EN']) +
        len(csw_ids_to_update_by_lang['FR']))
    if not count_csw_ids_to_update:
        zero_count += 1
    LOGGER.info(
        'Number of Open Data items in AGOL account to update: %s',
        count_csw_ids_to_update)
    removed_csw_ids = get_removed_csw_ids(csw_ids, agol_items_by_csw_id_lang)
    if not removed_csw_ids:
        zero_count += 1
    LOGGER.info(
        'Number of AGOL items to remove: %s', len(removed_csw_ids))
    if status_only:
        return True
    if zero_count == 3:
        LOGGER.info('--------------------------')
        LOGGER.info('No changes required')
        LOGGER.info('--------------------------')
        return True
    if count_csw_ids_to_update:
        LOGGER.info('--------------------------')
        LOGGER.info('UPDATING EXISTING ITEMS')
        LOGGER.info('--------------------------')

        result = update_agol_items_from_csw_info(
            csw_ids_to_update_by_lang, agol_items_by_csw_id_lang)
        if not result:
            LOGGER.error('... failure in updating existing items')
    if update_only:
        return True
    if new_csw_ids:
        LOGGER.info('--------------------------')
        LOGGER.info('ADDING NEW ITEMS')
        LOGGER.info('--------------------------')
        i = 0
        for csw_id in csw_ids:
            if csw_id not in new_csw_ids:
                continue
            i += 1
            LOGGER.info('%s: %s', i, csw_id)
            # LOGGER.info(open_data_records[csw_id]['title'])
            #
            if 'EN' in new_csw_ids[csw_id] and 'FR' in new_csw_ids[csw_id]:
                agol_ids = add_csw_item_to_agol(csw_id)
            else:
                lang_to_process = new_csw_ids[csw_id][0]
                agol_ids = add_csw_item_to_agol(csw_id, lang=lang_to_process)
            if not agol_ids:
                LOGGER.error('... failed adding record to ArcGIS Online')
    if not removed_csw_ids:
        return True
    if delete_items:
        LOGGER.info('--------------------------')
        LOGGER.info('DELETING REMOVED ITEMS')
        LOGGER.info('--------------------------')
        delete_agol_items_based_on_csw_id(
            removed_csw_ids, agol_items_by_csw_id_lang)
    else:
        LOGGER.info('--------------------------')
        LOGGER.info('AGOL ITEMS TO BE DELETED')
        LOGGER.info('(use parameter \'-d\' to delete)')
        LOGGER.info('--------------------------')
        for csw_id in removed_csw_ids:
            if csw_id not in agol_items_by_csw_id_lang:
                # this shouldn't happen
                continue
            for lang in ['EN', 'FR']:
                if not agol_items_by_csw_id_lang[csw_id][lang]:
                    continue
                agol_id = agol_items_by_csw_id_lang[csw_id][lang]['id']
                agol_url = AGOL_ITEM_URL + agol_id
                LOGGER.info(agol_url)
                webbrowser.open(agol_url)
                webbrowser.open(OPENDATA_ITEM_URL[lang] + csw_id)

    LOGGER.info('--------------------------')
    LOGGER.info('FINISHED')
    LOGGER.info('--------------------------')
    return True


if __name__ == '__main__':
    main()

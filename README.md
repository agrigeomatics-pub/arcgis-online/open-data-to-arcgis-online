Open Data to ArcGIS Online
==========================
Add mappable items from the Canada Open Data portal to ArcGIS Online.

Find all web mapping services on the public Open Data site and
add them to ArcGIS Online as new items within folders named after official
department acroynms (e.g. "AAFC" for Agricultural and Agri-Food Canada,
"TC" for Transport Canada, etc.).

Open Data portal: https://open.canada.ca/en/open-data

Command-line only.

### Requirements
- Python 3.8+
- Python modules:
  - arcgis
  - requests
- ArcGIS Online account

### Environment setup
The recommended method to setup a working Python environment install Anaconda or Miniconda.

(Note if ArcGIS Pro is installed then Anaconda is already installed.) 

#### Automated
(Windows only)

Run the batch file "opendata_to_agol.bat". It will install a working Python environment with
the required modules, and then run the Python script.

    $ opendata_to_agol.bat

#### Manual

(Windows or Linux)

Use the included "environment.yml" file to create the required conda environment.

    $ conda env create --name pyarcgis --file environment.yml
    $ activate pyarcgis

... or

    $ conda create --name pyarcgis python=3.9
    $ activate pyarcgis
    $ conda config --env --append conda-forge
    $ conda config --env --append esri
    $ conda install arcgis requests

### Running the script

#### Add Open Data items from one organization

Use the organization's acronym as described by the Treasury Board of Canada or
the two-letter abbreviation for a Canadian province or territory.

https://www.tbs-sct.canada.ca/ap/fip-pcim/reg-eng.asp?printable=true

https://www150.statcan.gc.ca/n1/pub/92-195-x/2011001/geo/prov/tbl/tbl8-eng.htm

e.g.
Add all items from Agriculture and Agri-Food Canada

    $ python opendata_to_agol.py --org AAFC

Add all items from Government of Quebec

    $ python opendata_to_agol.py --org QC


#### Add all items from Open Data
<b>*** Warning: This will add thousands of new items to your AGOL organization. *** </b>

    $ python opendata_to_agol.py --all

#### Faster processing
During the initial run the script saves the discovered Open Data content to a local file.
Subsequent runs can use this file to avoid accessing Open Data again by including the
`--file` parameter. This is useful when if the script is adding a larger number of items 
but fails during the process.

(Note new content added to Open Data between runs will not be discovered when 
using his parameter.)

    $ python opendata_to_agol.py --org AAFC --file

### ArcGIS Online credentials
The script will request an ArcGIS Online username and password each time it is run.
To avoid typing it in manually for every run, create a `credentials.py` file in the same path
as the script with these two lines:

    USERNAME = '<username>'
    PASSWORD = '<password>'


### Parameters

    $ python opendata_to_agol.py --help

    opendata_to_agol.py [-h] [--all] [--debug] [--delete] [--doctest] [--files]
                        [--org [ORG]] [--status] [--update] [--validate]

    Copy Open Data items to ArcGIS Online

    optional arguments:
      -h, --help            show this help message and exit
      --all, -a             Add Open Data items from all departments.
      --debug, -db          Run in debug mode.
      --delete, -d          Delete obsolete ArcGIS Online items.
      --doctest, -dt        Run the doctests.
      --files, -f           Load Open Data content saved from the previous run.
      --org [ORG], -o [ORG]
                            Specific organization acronym with Open Data items.
      --status, -s          Compare Open Data content with AGOL content. No items added.
      --update, -u          Update existing ArcGIS Online items only, do not add new items.
      --validate, -v        Validate existing ArcGIS Online items.

## Licence
Unless otherwise noted, the source code of this project is covered under Crown Copyright, Government of Canada, and is distributed under the MIT License.

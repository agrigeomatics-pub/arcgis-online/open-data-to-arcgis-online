"""
Build a Government of Canada department acronym dictionary.

Acronymns are extracted from the following site:
https://www.tbs-sct.canada.ca/ap/fip-pcim/reg-eng.asp?printable=true
"""
import time
import urllib.request
from html.parser import HTMLParser


class BuildDictionary(HTMLParser):
    """
    Class to parse a GoC webpage to create a department acronym dictionary.

    The dictionary follows this format:
    <department title>: {'EN':<english acronym>, 'FR':<French acronym>}

    Every department will have two entries with the same acronym
    values (one for each language).

    e.g.
    {
        'Agriculture and Agri-Food Canada': {'EN': 'AAFC', 'FR': 'AAC'},
        'Agriculture et Agroalimentaire Canada': {'EN': 'AAFC', 'FR': 'AAC'},
        ...
    }
    """

    def __init__(self):
        """Initialize class."""
        super().__init__()
        self.reset()
        self.acronym_dict = {}
        self.title_dict = {'EN': [], 'FR': []}
        self.inside_table = False
        self.record = False
        self.row_values = []

    def handle_starttag(self, tag, attrs):
        """
        Process start tags to determine when to record data.

        It starts recording data when it's inside a table and inside
        a table row.
        """
        if tag == 'tbody':
            self.inside_table = True
        if self.inside_table and tag == 'tr':
            self.row_values = []
        if self.inside_table and tag == 'td':
            self.record = True

    def handle_endtag(self, tag):
        """
        Process end tags to determine when to stop recording data.

        It stops recording when it's outside of a table or it's at the
        end of a row.
        At the end of the row it adds the data to the output dictionary.
        """
        if tag == 'tbody':
            self.inside_table = False
            self.record = False
            return
        if (
                not self.inside_table or
                not self.record or
                tag == 'td' or
                tag != 'tr'):
            return
        # Rows with less than four values are incomplete and will be ignored.
        if len(self.row_values) < 4:
            return
        title_en, title_fr, abbr_en, abbr_fr = self.row_values[-4:]
        if title_en.isnumeric() or title_en == 'Footnote':
            return
        self.acronym_dict[title_en] = {'EN': abbr_en, 'FR': abbr_fr}
        self.acronym_dict[title_fr] = {'EN': abbr_en, 'FR': abbr_fr}
        self.title_dict['EN'].append(title_en)
        self.title_dict['FR'].append(title_fr)
        self.row_values = []
        self.record = False

    def handle_data(self, data):
        """Process data when the recording flag is set to true."""
        if not self.inside_table or not self.record:
            return
        if not data.strip():
            return
        self.row_values.append(data.strip())


def get_dept_dicts():
    """Build dictionaries of English and French GoC titles and acronyms."""
    url = 'https://www.tbs-sct.canada.ca/ap/fip-pcim/reg-eng.asp?printable=true'
    urlobj = None
    for i in range(1, 6):
        try:
            urlobj = urllib.request.urlopen(url)
            break
        except urllib.error.URLError:
            print(
                'Failed accessing department acronym URL,' +
                ' attempt {} of 5'.format(i))
            time.sleep(5)
    if not urlobj:
        return None
    html = urlobj.read().decode()
    parser = BuildDictionary()
    parser.feed(html)
    return parser.acronym_dict, parser.title_dict


if __name__ == '__main__':
    get_dept_dicts()
